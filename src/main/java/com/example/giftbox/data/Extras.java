package com.example.giftbox.data;

public class Extras {
//    @PrimaryKey
//    @ColumnInfo(name = "user_id")
//    @SerializedName("user_id")
    private String user_id;
//    @SerializedName("message")
    private int message;
//    @SerializedName("success")
    private int success;

    public Extras(String user_id, int message) {
        this.user_id = user_id;
        this.message = message;
    }

    public Extras(String user_id, int message, int success) {
        this.user_id = user_id;
        this.message = message;
        this.success = success;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
