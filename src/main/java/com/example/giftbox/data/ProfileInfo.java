package com.example.giftbox.data;

import java.util.List;

public class ProfileInfo {
//    @SerializedName("users")
    private List<User> users = null;
//    @SerializedName("extras")
    private List<Extras> extras = null;
//    @SerializedName("events")
//    private List<Events> events = null;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers (List<User> users) {
        this.users = users;
    }

    public List<Extras> getExtras() {
        return extras;
    }

    public void setExtras(List<Extras> extras) {
        this.extras = extras;
    }

    /*public List<Events> getEvents() {
        return events;
    }

    public void setEvents(List<Events> events) {
        this.events = events;
    }*/
}
