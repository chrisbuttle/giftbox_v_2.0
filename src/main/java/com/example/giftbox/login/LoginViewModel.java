package com.example.giftbox.login;

import android.util.Log;
import android.util.Patterns;
import android.view.View;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

//import com.example.giftbox.DaggerLoginRepoComponent;
//import com.example.giftbox.dagger.LoginRepoComponent;
import com.example.giftbox.di.DaggerLoginRepoComponent;
import com.example.giftbox.di.LoginRepoComponent;
import com.example.giftbox.repo.Repository;

public class LoginViewModel extends ViewModel {

    //    https://medium.com/@harivigneshjayapalan/dagger-2-for-android-beginners-advanced-part-i-1e14fccf2cc8
    private final String TAG = this.getClass().getSimpleName();

    //field injection of repository
    @Inject
    Repository repository;

    public MutableLiveData<String> UserEmailAddress = new MutableLiveData<>();
    public MutableLiveData<String> UserPassword = new MutableLiveData<>();
    private MutableLiveData<Integer> messageMutableLiveData;
    private MutableLiveData<Boolean> exitMutableLiveData;

    public LoginViewModel() {
        super();
//        repository = new Repository();
        //create di component
        LoginRepoComponent component = DaggerLoginRepoComponent.create();
        //tell dagger which activity to pass repository (above) into (field injection) - https://www.youtube.com/watch?v=tgY4Jw8OFZI
        component.injectLoginViewModel(this);
    }

    public MutableLiveData<Integer> getMessage() {
        if (messageMutableLiveData == null) {
            messageMutableLiveData = new MutableLiveData<>();
        }
        return messageMutableLiveData;
    }

    public MutableLiveData<Boolean> exitApplication() {
        if (exitMutableLiveData == null) {
            exitMutableLiveData = new MutableLiveData<>();
        }
        return exitMutableLiveData;
    }

    public void loginOnClick(View view) {
        Log.i(TAG, "HI");
        String user_email = UserEmailAddress.getValue();
        String user_password = UserPassword.getValue();
        //TODO check if fields are empty and send message to view
        if ((user_email == null || (user_password == null))) {
            messageMutableLiveData.setValue(4);
        } else {
            if (isEmailValid(user_email)) {
                Log.i(TAG, "Is Valid Email");
                repository.getInfo(messageMutableLiveData, user_email, user_password);
            } else {
                Log.i(TAG, "Is Invalid Email");
                messageMutableLiveData.setValue(5);
            }
        }
    }

    public void cancelOnClick(View view) {
        Log.i(TAG, "cancelOnClick: ");
        exitMutableLiveData.setValue(true);
    }

    private boolean isEmailValid(String userEmail) {
        return Patterns.EMAIL_ADDRESS.matcher(userEmail).matches();
    }

}
