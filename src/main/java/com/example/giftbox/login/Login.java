package com.example.giftbox.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.giftbox.di.DaggerLoginRepoComponent;
import com.example.giftbox.di.LoginRepoComponent;
import com.example.giftbox.MessageSelector;
import com.example.giftbox.R;
import com.example.giftbox.databinding.ActivityLoginBinding;
import com.example.giftbox.index.Index;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

public class Login extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    private LoginViewModel loginViewModel;
    private ActivityLoginBinding binding;

    @Inject
    MessageSelector messageSelector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        binding = DataBindingUtil.setContentView(Login.this, R.layout.activity_login);
        binding.setLifecycleOwner(this);
        binding.setLoginViewModel(loginViewModel);

        LoginRepoComponent component = DaggerLoginRepoComponent.create();
        component.injectLogin(this);

        loginViewModel.getMessage().observe(this, new Observer<Integer>() {

            @Override
            public void onChanged(Integer messageInt) {
                Log.i(TAG, "Message Changed" + messageInt);
                showResult(messageInt);
            }
        });

        loginViewModel.exitApplication().observe(this, new Observer<Boolean>() {

            @Override
            public void onChanged(Boolean b) {
                Log.i(TAG, "Login - onChanged: ");
                if (b) {
                    finish();
                }
            }
        });
    }//ends onCreate

    private void showResult(int messageInt) {
        String messageString = messageSelector.displayMessage(messageInt);
//        Toast.makeText(Login.this, messageString, Toast.LENGTH_LONG).show();
        if (messageInt == 1) {
            login();
            Log.i(TAG, "showResult: login");
        } else if (messageInt == 2) {
            //only password
            binding.enterPassword.setError(messageString);
            //only email
        } else if ((messageInt == 3) || (messageInt == 5)) {
            binding.enterEmail.setError(messageString);
            //pass and email
        } else if (messageInt == 4) {
            binding.enterPassword.setError(messageString);
            binding.enterEmail.setError(messageString);
        }
    }

    private void login() {
        Log.i(TAG, "logging in..");
        Intent intent = new Intent(Login.this, Index.class);
        Bundle extras = new Bundle();
        extras.putString("go", "GO");
        intent.putExtras(extras);
        startActivity(intent);
        finish();
    }
}
