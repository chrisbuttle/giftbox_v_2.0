package com.example.giftbox.dialogs;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ExitDialogViewModel extends ViewModel {
    private final String TAG = this.getClass().getSimpleName();
    //    for single event use the following tutorial, see link to github
//    https://medium.com/androiddevelopers/livedata-with-snackbar-navigation-and-other-events-the-singleliveevent-case-ac2622673150
//    or
//    https://github.com/googlesamples/android-architecture/blob/dev-todo-mvvm-live/todoapp/app/src/main/java/com/example/android/architecture/blueprints/todoapp/SingleLiveEvent.java
    private MutableLiveData<Boolean> liveData;

    public ExitDialogViewModel() {
        liveData = new MutableLiveData<>();
        liveData.setValue(false);
    }

    public MutableLiveData<Boolean> clickToggle() {
        return liveData;
    }

    public void onClick(View view) {
        String message= "clicked";
        Log.i(TAG, "onClick: Clicked");
//        onInputListener.sendInput(message);
        liveData.setValue(true);
    }
}
