package com.example.giftbox.dialogs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.giftbox.OnExitListener;
import com.example.giftbox.R;
import com.example.giftbox.databinding.ExitDialogBinding;

import javax.inject.Inject;

public class ExitDialog extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();

    private ExitDialogViewModel exitDialogViewModel;

    private OnExitListener onExitListener;

    @Inject
    public ExitDialog() {
    }

    //    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ExitDialogBinding binding = DataBindingUtil.inflate(inflater, R.layout.exit_dialog, container, false);
        binding.setLifecycleOwner(this);
        exitDialogViewModel = ViewModelProviders.of(this).get(ExitDialogViewModel.class);
        binding.setExitDialogViewModel(exitDialogViewModel);
//        TODO :dialog.setCanceledOnTouchOutside(false);

        exitDialogViewModel.clickToggle().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                onExitListener = (OnExitListener) getActivity();
                final String message = "message";
                if (aBoolean) {
                    Log.i(TAG, "onChanged = tue");
                    onExitListener.onExit(message);
                    dismiss();
                }
            }
        });
        return binding.getRoot();
    }

   /* @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {

        } catch (ClassCastException e) {
            Log.i(TAG, "ClassException" + e.getMessage());
        }
    }*/

    @Override
    public void dismiss() {
        super.dismiss();
    }

}
