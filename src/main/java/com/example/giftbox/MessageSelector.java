package com.example.giftbox;

import android.content.Context;

import com.example.giftbox.app.GlobalApplication;

import javax.inject.Inject;

public class MessageSelector {

    private Context context;
    @Inject
    public MessageSelector() {
        context = GlobalApplication.getAppContext();
//        TODO application context working??
    }

    public String displayMessage(int messageInt) {
        String result_message;
        switch(messageInt) {
            case 1:
                result_message = context.getString(R.string.welcome_message);
                break;
            case 2:
                result_message = context.getString(R.string.password_incorrect);
                break;
            case 3:
                result_message = context.getString(R.string.email_register);
                break;
            case 4:
                result_message = context.getString(R.string.form_fields_error);
                break;
            case 5:
                result_message = context.getString(R.string.email_valid);
                break;
            case 6:
                result_message = context.getString(R.string.db_result_error);
                break;
            case 7:
                result_message = context.getString(R.string.network_fail);
                break;
            default:
                result_message = context.getString(R.string.db_result_error);
        }
        return result_message;
    }

}
