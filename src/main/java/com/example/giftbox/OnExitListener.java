package com.example.giftbox;

public interface OnExitListener {
    void onExit(String message);
}
