package com.example.giftbox.index;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.giftbox.data.User;
import com.example.giftbox.di.DaggerLoginRepoComponent;
import com.example.giftbox.di.LoginRepoComponent;
import com.example.giftbox.repo.Repository;

import javax.inject.Inject;

public class IndexViewModel extends ViewModel {

    private final String TAG = this.getClass().getSimpleName();
    private LiveData<User> userProfileLiveData;
    private MutableLiveData<Boolean> exitDialog;

    @Inject
    Repository repository;

    public IndexViewModel() {
        super();
        LoginRepoComponent component = DaggerLoginRepoComponent.create();
        component.injectIndex(this);
        userProfileLiveData = repository.getUserProfile();
    }

    public LiveData<User> getProfile() {
        return userProfileLiveData;
    }

    public MutableLiveData<Boolean> callDialog() {
        exitDialog = new MutableLiveData<>();
        return exitDialog;
    }

    public void onClick(View view) {
        Log.i(TAG, "onClick");
        exitDialog.setValue(true);
    }

}
