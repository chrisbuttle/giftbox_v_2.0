package com.example.giftbox.index;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.giftbox.OnExitListener;
import com.example.giftbox.dialogs.ExitDialog;
import com.example.giftbox.R;
import com.example.giftbox.data.User;
import com.example.giftbox.databinding.ActivityIndexBinding;
import com.example.giftbox.di.DaggerLoginRepoComponent;
import com.example.giftbox.di.LoginRepoComponent;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;


public class Index extends AppCompatActivity implements OnExitListener {
//    TODO test sending image to next activity as its set up now
//    TODO download and cache image from server, save as in file??

    private final String TAG = this.getClass().getSimpleName();
    private IndexViewModel indexViewModel;
    private ActivityIndexBinding binding;

    @Inject
    ExitDialog exitDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        indexViewModel = ViewModelProviders.of(this).get(IndexViewModel.class);

        binding = DataBindingUtil.setContentView(Index.this, R.layout.activity_index);
        binding.setLifecycleOwner(this);
        binding.setIndexViewModel(indexViewModel);

        LoginRepoComponent component = DaggerLoginRepoComponent.create();
        component.injectIndex(this);

        indexViewModel.getProfile().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                showResult(user);
            }
        });

        indexViewModel.callDialog().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean b) {
                Log.i(TAG, "onClick: opening exit dialog.");
                exitDialog.show(getSupportFragmentManager(), "AlertDialog");
            }
        });
    }

    private void showResult(@NonNull User user) {
        loadImage(user.getUserImageUrl());
        binding.userNameView.setText(user.getUserName());
        binding.userEmailView.setText(user.getUserEmail());
        binding.birthdateView.setText(user.getUserBirthDate());
    }

    private void loadImage(String image_url) {
        Picasso.get().setIndicatorsEnabled(true);
        Picasso.get().load(image_url).placeholder(R.drawable.default_info_panel_icon_user)
                .error(R.drawable.default_info_panel_icon_user).into(binding.userImageView);
    }

    @Override
    public void onExit(String message) {
        Toast.makeText(Index.this, message, Toast.LENGTH_LONG).show();
        finish();
    }
}
