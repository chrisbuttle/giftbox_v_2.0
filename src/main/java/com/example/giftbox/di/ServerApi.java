package com.example.giftbox.di;

import com.example.giftbox.data.ProfileInfo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServerApi {
    @FormUrlEncoded
    @POST("gb_login.php")
    Call<ProfileInfo> getJsonObjectData(
            @Field("login_submit") String login_submit,
            @Field("login_email") String login_email,
            @Field("login_pass") String login_pass
    );
}
