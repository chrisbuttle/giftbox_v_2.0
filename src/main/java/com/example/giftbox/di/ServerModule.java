package com.example.giftbox.di;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ServerModule {
//    module for all network connecting methods
    @Provides
    public ServerApi serverApi(Retrofit retrofit){
        return retrofit.create(ServerApi.class);
    }

    //    Not converting to Gson, using Json String instead
    @Provides
    public Retrofit retrofit(){
        return new Retrofit.Builder()
                .baseUrl("http://******/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
