package com.example.giftbox.di;

import com.example.giftbox.index.Index;
import com.example.giftbox.index.IndexViewModel;
import com.example.giftbox.login.Login;
import com.example.giftbox.login.LoginViewModel;

import dagger.Component;

@Component(modules = ServerModule.class)
public interface LoginRepoComponent {

    void injectLogin(Login view);
    void injectLoginViewModel(LoginViewModel viewModel);
    void injectIndex(Index view);
    void injectIndex(IndexViewModel viewModel);
//    void injectRepository(Repository repository);
    ServerApi getService();
}
