package com.example.giftbox.repo;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.giftbox.data.User;
import com.example.giftbox.database.ProfileDao;
import com.example.giftbox.database.ProfileRoomDatabase;
import com.example.giftbox.di.DaggerLoginRepoComponent;
import com.example.giftbox.di.LoginRepoComponent;
import com.example.giftbox.di.ServerApi;
import com.example.giftbox.app.GlobalApplication;
import com.example.giftbox.data.ProfileInfo;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
//TODO error handling for retrofit and room
    private static final String TAG = Repository.class.getSimpleName();

    private Context context;
    private ProfileInfo profileInfo;
    private ProfileDao profileDao;
     private LiveData<User> userLiveData;
    ServerApi serverApi;

    @Inject
    public Repository() {
        context = GlobalApplication.getAppContext();
        ProfileRoomDatabase db = ProfileRoomDatabase.getDatabase(context);
        profileDao = db.profileDao();
    }

    public LiveData<User> getUserProfile() {
        userLiveData = profileDao.getProfile();
        return userLiveData;
    }

    private void saveToLocalDb(User userProfileData) {
        new insertAsyncTask(profileDao).execute(userProfileData);
    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {
        private ProfileDao mAsyncTaskDao;

        insertAsyncTask(ProfileDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            Log.i(TAG, "Insert D I B");
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void getInfo(final MutableLiveData<Integer> userMessage, String loginUserEmail, String loginUserPassword) {
        Log.i(TAG, "getInfo" + "Connecting to a Network!");
        LoginRepoComponent component = DaggerLoginRepoComponent.create();
        serverApi = component.getService();

        Call<ProfileInfo> call = serverApi.getJsonObjectData("login_submit", loginUserEmail, loginUserPassword);
        call.enqueue(new Callback<ProfileInfo>() {
            @Override
            public void onResponse(@NotNull Call<ProfileInfo> call, @NotNull Response<ProfileInfo> response) {
//                TODO 1 database, 3 tables
//                TODO get presents etc
                if (response.isSuccessful()) {
                    Log.i(TAG, "response is successful");
                    try {
                        profileInfo = response.body();
                        if (profileInfo == null) {
                            Log.i(TAG, "NULL");
                            userMessage.setValue(6);
                        } else {
                            Log.i(TAG, "NOT NULL");
                            String name = profileInfo.getUsers().get(0).getUserName();
                            Log.i(TAG, name);
                            int messageCode = profileInfo.getExtras().get(0).getMessage();
                            if (messageCode == 1) {
                                Log.i(TAG, "saving to local db" + messageCode);
                                try {
                                    saveToLocalDb(profileInfo.getUsers().get(0));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    userMessage.setValue(6);
                                }
                                userMessage.setValue(messageCode);
                            } else {
                                Log.i(TAG, String.valueOf(messageCode));
                                userMessage.setValue(messageCode);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        userMessage.setValue(6);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileInfo> call, @NotNull Throwable t) {
                Log.e(TAG, "getInfo" + "NETWORK CALL FAIL " + t.toString());
//                userMessage.setValue(5);
            }
        });
    }
}
