package com.example.giftbox.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.giftbox.data.User;

@Dao
public interface ProfileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Update
    void updateUser(User user);

    @Query("SELECT * from user")
    LiveData<User> getProfile();

    @Query("DELETE FROM user")
    void nukeTable();
}
