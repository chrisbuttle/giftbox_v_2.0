package com.example.giftbox.index;

import com.example.giftbox.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withHint;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class IndexTest {
    private final String TAG = this.getClass().getSimpleName();

    @Rule
    public ActivityTestRule<Index> activityTestRule = new ActivityTestRule<>(Index.class);


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void checkTextView() {
        Espresso.onView(withText("Title")).check(matches(isDisplayed()));
        Espresso.onView(withText("Message")).check(matches(isDisplayed()));
        Espresso.onView(withText("Name")).check(matches(isDisplayed()));
        Espresso.onView(withText("Email")).check(matches(isDisplayed()));
        Espresso.onView(withText("Date of Birth")).check(matches(isDisplayed()));
    }

    @Test
    public void checkText() {
        Espresso.onView(withId(R.id.userNameView)).check(matches(withText("Name")));
    }

    @After
    public void tearDown() throws Exception {
    }
}