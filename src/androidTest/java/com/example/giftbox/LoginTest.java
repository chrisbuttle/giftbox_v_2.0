package com.example.giftbox;

import android.content.Context;
import android.util.Log;

import com.example.giftbox.app.GlobalApplication;
import com.example.giftbox.login.Login;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class LoginTest {
    private final String TAG = this.getClass().getSimpleName();
    private MessageSelector messageSelector;
    private Context context;
    private List<String> answerList;

    @Rule
    public ActivityTestRule<Login> activityTestRule = new ActivityTestRule<>(Login.class);
    private Login login = null;


    @Before
    public void setUp() throws Exception {
        context = GlobalApplication.getAppContext();
        String testString = context.getString(R.string.welcome_message);
        Log.i(TAG, testString);
        login = activityTestRule.getActivity();
        messageSelector = new MessageSelector();
//        Make String for answers
        answerList = new ArrayList<>();
        answerList.add(context.getString(R.string.welcome_message));
        answerList.add(context.getString(R.string.password_incorrect));
        answerList.add(context.getString(R.string.email_register));
        answerList.add(context.getString(R.string.form_fields_error));
        answerList.add(context.getString(R.string.email_valid));
        answerList.add(context.getString(R.string.db_result_error));
        answerList.add(context.getString(R.string.network_fail));
    }

    @Test
    public void ensureTextView() {
        //enter value in edittext
        onView(withId(R.id.enterEmail)).perform(typeText("chris.buttle@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.enterPassword)).perform(typeText("giftbox"), closeSoftKeyboard());
//        peformclick
        onView(withId(R.id.loginButton)).perform(click());
        onView(withId(R.id.cancelButton)).perform(click());
        onView(withId(R.id.registerButton)).perform(click());
    }

    @Test
    public void showMessageResult() {
        for(int i = 0; i < answerList.size(); i++) {
//            int messageInt = i;
            String actual = messageSelector.displayMessage(i + 1);
            String expected = answerList.get(i);
            assertEquals("Welcome message 1", expected, actual);
        }

    }

    @After
    public void tearDown() throws Exception {
        login = null;
    }
}